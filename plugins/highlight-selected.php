<?php
/**
 * @author Yusup Hambali
 * @license BSD
 */
class HighlightSelected
{
	public function head()
	{
		if (!DB) {
//			return;
		}
    if (!isset($_GET['sql'])) {
      return;
    }
    preg_match('/(INSERT INTO|SELECT.+?FROM|UPDATE).+?`(\w+)`/si', $_GET['sql'], $matches);
    
    if (!$table = $matches[2] ?? null) {
      return;
    } ?>
<script<?= nonce();?> type="text/javascript">
document.addEventListener('DOMContentLoaded', function(lis) {
  lis = document.querySelectorAll('#tables li')
  lis.forEach(e => {
    if (!e.innerHTML.match(/select=<?= $table ?>/)) {
      return
    }
    e.querySelectorAll('a')[1].classList.add('active')
    return false
  })
})
</script>
<?php
	}
}
